import os
from typing import Any

import pandas as pd

from dataset.parser.csv_parser_interface import CsvParserInterface
from dataset.parser.yt_videos_segments_csv_parser import YtVideosSegmentsCsvParser
from dataset.processing.dataset_processor_base import (
    DatasetProcessorBase,
    ProcessorConfiguration,
)
from dataset.video.downloader.segment_downloader_interface import (
    SegmentDownloaderInterface,
)
from dataset.video.downloader.supplementary_types import DownloaderOptions
from dataset.video.downloader.yt_segment_downloader import YtSegmentDownloader
from dataset.video.processing.ffmpeg_frames_extractor import FfmpegFramesExtractor
from dataset.video.processing.frames_extractor_interface import FramesExtractorInterface
from logger import logger
from utils.archiver import Archiver


class VideosToFramesConverter(DatasetProcessorBase):
    def __init__(
        self,
        yt_cookiefile_path=None,
        configuration: ProcessorConfiguration = ProcessorConfiguration(),
        csv_parser: CsvParserInterface = YtVideosSegmentsCsvParser(),
        segment_downloader: SegmentDownloaderInterface = YtSegmentDownloader(),
        downloader_options: DownloaderOptions = DownloaderOptions(),
        archiver=Archiver(),
        frames_extractor: FramesExtractorInterface = FfmpegFramesExtractor(),
    ):
        super().__init__(
            yt_cookiefile_path,
            configuration,
            csv_parser,
            segment_downloader,
            downloader_options,
            archiver,
        )

        self._frames_extractor = frames_extractor

    def _convert_video_to_frames(self, video_path):
        logger.debug(f"video_path: {video_path}")
        frames_paths = self._frames_extractor.extract_frames(
            video_path, self._configuration.frames_per_second
        )
        self._remove_video(video_path)
        return frames_paths

    def _process_single_video(
        self, output_dir_path: str, data_row: pd.DataFrame
    ) -> Any:
        try:
            video_path = self._download_video(
                data_row["yt_id"],
                data_row["segment_start_seconds"],
                data_row["segment_end_seconds"],
                output_dir_path,
            )
            frames_paths = self._convert_video_to_frames(video_path)
            logger.info(f"Frame paths: {frames_paths}")
            return frames_paths
        except Exception as e:
            logger.error(f'{data_row["yt_id"]} error: {e}')
            self._failed_to_process_list.append(data_row["yt_id"])
            return []

    def _postprocess(self, processing_outputs, output_dir_path):
        self._compress_frames(processing_outputs, output_dir_path)

    def _compress_frames(self, frames_paths, output_dir_path):
        logger.info(
            f"frames_paths: {frames_paths}, output_dir_path: {output_dir_path},"
            f" batch_counter: {self._batch_counter}"
        )
        self._archiver.compress(
            frames_paths,
            str(self._batch_counter),
            output_dir_path,
            remove_original_files=True,
        )

    def _remove_video(self, video_path):
        os.remove(video_path)
