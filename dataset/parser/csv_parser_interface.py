from abc import ABC, abstractmethod

import pandas as pd


class CsvParserInterface(ABC):
    @abstractmethod
    def parse_csv(self, path_to_csv: str) -> pd.DataFrame:
        pass
