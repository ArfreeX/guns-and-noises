import os
import subprocess
from typing import List

from dataset.video.downloader.supplementary_types import MediaMergingError
from logger import logger


class FfmpegStreamsMerger(object):
    def merge_video_and_audio(self, segments_paths: List[str]) -> str:
        logger.info(f"segments_paths: {segments_paths}")
        path_dirname = os.path.dirname(segments_paths[0])
        path_basename = os.path.basename(segments_paths[0])
        output_path = os.path.join(
            path_dirname, self._get_name_without_suffix(path_basename) + "_merged.mp4"
        )
        cmd = f'ffmpeg -y -i "{segments_paths[0]}" -i "{segments_paths[1]}" -c:v copy -c:a aac "{output_path}"'
        logger.info(f"calling: {cmd}")
        with subprocess.Popen(
            cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        ) as proc:
            proc_stdout, _ = proc.communicate()
            logger.info(proc_stdout)
            if proc.returncode != 0:
                raise MediaMergingError(
                    f"Non zero return code: {proc.returncode} from ffmpeg cmd: {cmd}"
                )

            self._remove_single_segments(segments_paths)

        return output_path

    def _get_name_without_suffix(self, name):
        index = name.rfind("_")
        return name[0:index]

    def _remove_single_segments(self, segments_paths):
        os.remove(segments_paths[0])
        os.remove(segments_paths[1])
