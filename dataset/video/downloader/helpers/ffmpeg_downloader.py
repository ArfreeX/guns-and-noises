import os
import subprocess

from dataset.video.downloader.supplementary_types import (
    DownloaderOptions,
    DownloadError,
    StreamsInfo,
)
from logger import logger


class FfmpegDownloader(object):
    def download_segment(
        self,
        identifier: str,
        streams_info: StreamsInfo,
        segment_start_seconds: float,
        segment_end_seconds: float,
        options: DownloaderOptions,
    ):
        if streams_info.is_audio_video_stream and (
            options.download_video or options.download_audio
        ):
            return [
                self._download_media_from_url(
                    identifier,
                    streams_info.video_stream_url,
                    segment_start_seconds,
                    segment_end_seconds,
                    options.output_dir_path,
                )
            ]

        partial_result = []
        if options.download_video:
            partial_result.append(
                self._download_media_from_url(
                    identifier + "_video",
                    streams_info.video_stream_url,
                    segment_start_seconds,
                    segment_end_seconds,
                    options.output_dir_path,
                )
            )
        if options.download_audio:
            partial_result.append(
                self._download_media_from_url(
                    identifier + "_audio",
                    streams_info.audio_stream_url,
                    segment_start_seconds,
                    segment_end_seconds,
                    options.output_dir_path,
                )
            )

        return partial_result

    def _download_media_from_url(
        self,
        identifier,
        video_stream_url,
        segment_start_seconds,
        segment_end_seconds,
        output_dir_path,
    ):
        output_path = os.path.join(output_dir_path, identifier + ".mp4")
        duration = int(segment_end_seconds - segment_start_seconds)
        cmd = (
            f'ffmpeg -y -ss {self._format_time(int(segment_start_seconds))} -i "{video_stream_url}"'
            f" -t {self._format_time(duration)} -c:v copy -c:a aac {output_path}"
        )
        logger.info(f"calling: {cmd}")
        with subprocess.Popen(
            cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
        ) as proc:
            proc_stdout, _ = proc.communicate()
            logger.info(proc_stdout)
            if proc.returncode != 0:
                raise DownloadError(
                    f"Non zero return code: {proc.returncode} from ffmpeg cmd: {cmd}"
                )

        return output_path

    def _format_time(self, seconds):
        minutes = seconds // 60
        hours = minutes // 60
        return f"{hours:02d}:{minutes % 60:02d}:{seconds % 60:02d}"
