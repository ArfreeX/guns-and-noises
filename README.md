# guns-and-noises

## First time use
```Shell
# cwd is project root dir
git-lfs install && git-lfs pull
```

## Non python dependencies
ffmpeg program is required for video processing.  
Download it from https://launchpad.net/ubuntu/+source/ffmpeg
or use your package manager.  
For Debian based distributions:
```Shell
sudo apt update
sudo apt install ffmpeg
```

## Poetry basic usage
Create virtual environment (**venv**)
- python -m venv venv

Install **poetry** in created virtual environment:
- source venv/bin/activate
- pip install poetry

Now, as the **poetry** installed, we can add dependencies to our project:
- poetry add \<package name\>

To restore all dependencies from project
(assuming that the poetry was installed [see first steps]).
If poetry is not installed, install it from **pip** (pip install poetry) and go further.
- source venv/bin/activate
- poetry install

## YT Videos dataset as frames
Ask maintainers for dataset or download it yourself with convert_videos_to_frames.py script
```Shell
# assuming venv is active, ffmpeg is installed, cwd is project root dir
tar -xJvf dataset/data/yt_videos_dataset/yt_segments_csv_files.tar.xz -C dataset/data/yt_videos_dataset/extracted_csv_files/
# get youtube cookie as txt file, could be done with "Get cookies.txt" extension for google-chrome
python convert_videos_to_frames.py ./dataset/data/yt_videos_dataset/extracted_csv_files/gunshot_segments.csv <output_dir> <path_to_cookiefile>
```

## Download YT Videos (with audio)
Ask maintainers for dataset or download it yourself with download_videos_with_audio.py script
```Shell
# assuming venv is active, ffmpeg is installed, cwd is project root dir
tar -xJvf dataset/data/yt_videos_dataset/yt_segments_csv_files.tar.xz -C dataset/data/yt_videos_dataset/extracted_csv_files/
# get youtube cookie as txt file, could be done with "Get cookies.txt" extension for google-chrome
python download_videos_with_audio.py ./dataset/data/yt_videos_dataset/extracted_csv_files/gunshot_segments.csv <output_dir> <path_to_cookiefile>
```
